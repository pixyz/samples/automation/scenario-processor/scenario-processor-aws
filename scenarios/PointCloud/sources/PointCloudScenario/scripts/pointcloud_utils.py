import os

def get_extension(path):
    """
    Returns path extension like ".fbx"
    """
    return os.path.splitext(path)[1]

def is_identity(matrix):
    return matrix == [[ 1.,  0.,  0.], [ 0.,  1.,  0.], [ 0.,  0.,  1.]]

def list_intersection(list_a, list_b):
    list_to_return = list()
    for a in list_a:
        if a in list_b:
            list_to_return.append(a)
    return list_to_return

def list_difference(list_a, list_b):
    list_to_return = list()
    for a in list_a:
        if a not in list_b:
            list_to_return.append(a)
    return list_to_return

def point_sub(pt1, pt2):
    return geom.Point3(pt1.x - pt2.x, pt1.y - pt2.y, pt1.z - pt2.z)

def dot_product(pt1, pt2):
    return pt1.x * pt2.x + pt1.y * pt2.y + pt1.z * pt2.z

def vector_length(v):
    return math.sqrt(dot_product(v, v))

def get_size(occurrence):
    """
    Returns the bounding box diagonal size in mm
    """
    aabb = scene.getAABB([occurrence])
    return vector_length(point_sub(aabb.high, aabb.low))

def get_parts(occurrences):
    parts = list()
    for occ in occurrences:
        parts.extend(scene.getPartOccurrences(occ))
    return parts
