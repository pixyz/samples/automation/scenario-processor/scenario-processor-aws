from dataclasses import dataclass
import pointcloud_utils
import pxz
from pxz import *

@dataclass
class DecimationParameters:
    distance: float

decimationQualities = {
    0: DecimationParameters(1),
    1: DecimationParameters(0.83),
    2: DecimationParameters(0.66),
    3: DecimationParameters(0.5),
    4: DecimationParameters(0.33)
}

def importFile(ioOptions, transform):
    autoRotation_temp = core.getModuleProperty('IO', 'FlipCoordinateSystem')
    core.setModuleProperty('IO', 'FlipCoordinateSystem', str(transform.orientation[0] == 'automaticOrientation'))
    importFailed = False
    
    try:
        # put it in try catch block so we can reset IO module properties
        root = io.importScene(ioOptions.inputFile)
    except Exception as e:
        core.setInteractiveMode(True)
        core.message(e)
        core.setInteractiveMode(False)
        importFailed = True

    core.setModuleProperty('IO', 'FlipCoordinateSystem', autoRotation_temp)

    if importFailed: raise Exception("Import failure")

    if transform.orientation[0] == 'manualOrientation':
        if transform.orientation[1].zUp:
            scene.applyTransformation(root, [[1, 0, 0, 0], [0, 0, 1, 0], [0, -1, 0, 0], [0, 0, 0, 1]])

        if transform.orientation[1].leftHanded:
            matLeftHanded = [[-1.0,0.,0.,0.],[0.,1.,0.,0.],[0.,0.,1.,0.],[0.,0.,0.,1.]]
            scene.applyTransformation(root, geom.multiplyMatrices(scene.getLocalMatrix(root), matLeftHanded))

    if transform.scale != 1:
        scene.applyTransformation(root, [[transform.scale, 0, 0, 0], [0, transform.scale, 0, 0], [0, 0, transform.scale, 0], [0, 0, 0, transform.scale]])

    # Clean all matrices and remove useless instances
    scene.resetTransform(root, True, True, False)

    return root

def segment(root, n):
    aabb = scene.getAABB([root])
    volume = (aabb.high.x - aabb.low.x) * (aabb.high.y - aabb.low.y) * (aabb.high.z - aabb.low.z)
    voxelGridSize = max(1, min(80, n))  # 1 <= grid size <= 80
    voxelSize = pow(volume, 1/3) / voxelGridSize
    scene.mergeParts([root], 0)
    algo.voxelizePointClouds([root], voxelSize)

def initLods(root, n):

    parts = scene.getPartOccurrences(root)
    #scene.makeInstanceUnique(parts)
    lods = [list() for i in range(0, n)]

    #Declares array to prevent processing the same part in multiple instances
    processedParts=[]

    #For each part
    for partOccurrence in parts:
        
        #Gets part id
        if(not scene.hasComponent(partOccurrence, scene.ComponentType.Part)):
            continue
        
        part = scene.getComponent(partOccurrence, scene.ComponentType.Part)
        
        #Checks if already processed
        if part in processedParts: continue
        processedParts.append(part)
        
        partOccurrence = scene.getComponentOccurrence(part)	
        name = core.getProperty(partOccurrence, "Name")
        
        lod_0 = scene.createOccurrence(name + "_LOD0")
        scene.setParent(lod_0, partOccurrence, True)
        scene.setComponentOccurrence(part, lod_0)

        for i in range(n):
            lod_i = scene.createOccurrence(name + "_LOD" + str(i + 1))
            scene.setParent(lod_i, partOccurrence, True)
            scene.setComponentOccurrence(core.cloneEntity(part), lod_i)
            lods[i].append(lod_i)

    return lods

def optimizePointCloud(ioOptions, geometry, transform):
    
    root = importFile(ioOptions, transform)
    
    if geometry.segmentation > 1:
        segment(root, geometry.segmentation)

    if geometry.pointCloudDensity != 0: # != Maximum
        aabb = scene.getAABB([root])
        volume = (aabb.high.x - aabb.low.x) * (aabb.high.y - aabb.low.y) * (aabb.high.z - aabb.low.z)  
        # Decimate Point Cloud depending on size
        voxelSize = pow(volume, 1/3)
        splatSize = 0.0035
        # Compute distance between points
        distance = max(0.002 * voxelSize, voxelSize * splatSize) / decimationQualities[geometry.pointCloudDensity].distance
        algo.decimatePointClouds([root], distance)

        
    if geometry.generateCollider:
        proxy = process.proxyFromPointCloud([root], 25, ["No", 0], True)

    if geometry.lods[0] == "Yes":
        nlods = max(2, min(geometry.lods[1].createLods, 6))
        count = scene.getVertexCount([root], False, False, True)
        lods = initLods(root, nlods - 1)
        for i in range(len(lods)):
            decimatePointClouds(lods[i], count, i + 1)
            count = scene.getVertexCount(lods[i], False, False, True)

    if ioOptions.outputFile != -1:
        io.exportScene(ioOptions.outputFile, root)

def decimatePointClouds(parts, previousCount, lodId):
    aabb = scene.getAABB(parts)
    splatSize = 0.0035 # size is rato to screen height
    voxelSize = max(max(aabb.high.x - aabb.low.x, aabb.high.y - aabb.low.y), aabb.high.z - aabb.low.z)

    currentCount = previousCount

    distance = max(0.005 * voxelSize, lodId * (voxelSize * splatSize) / 0.95)
    nextDistance = distance

    k = 0
    while(currentCount > 0.5 * previousCount):
        distance = nextDistance
        algo.decimatePointClouds(parts, distance)
        currentPointCount = scene.getVertexCount(parts, False, False, True)
        nextDistance = distance * (max(0, 10 * pow(currentPointCount / previousCount - 0.4, 3)) + 1)
        k += 1
        if k > 3:
            # emergency exit
            break
    
    ratio = (voxelSize * splatSize) / distance
    for part in parts:
        core.addCustomProperty(part, 'PXZ_LOD_TRANSITION', str(ratio))


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

def convertToMesh(ioOptions, geometry, transform):
    root = importFile(ioOptions, transform)

    if geometry.segmentation > 1:
        segment(root, geometry.segmentation)

    for part in scene.getPartOccurrences():
        scene.select([part])
        process.proxyFromPointCloud([part], geometry.gridResolution, geometry.generateDiffuseTexture, False)
        scene.clearSelection()

    if ioOptions.outputFile != -1:
        io.exportScene(ioOptions.outputFile, scene.getRoot())
    

