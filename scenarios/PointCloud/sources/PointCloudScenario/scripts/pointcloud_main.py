from pointcloud_scenario import convertToMesh
from pointcloud_scenario import optimizePointCloud
import json
import os

def str2bool(v):
  return v.lower() in ("yes", "true", "t", "1")

def downloadFiles(ioOptionsAWS):
    ## S3 files download
    extension = os.path.splitext(ioOptionsAWS.input_s3_key)
    head, tail = os.path.split(ioOptionsAWS.input_s3_key)
    name = os.path.splitext(tail)

    outputHead, outputTail = os.path.split(ioOptionsAWS.output_s3_key)
    outputName = os.path.splitext(outputTail)
    
    localInputFolder = core.getTempDirectory() + '/input/'
    localOutputFolder = core.getTempDirectory() + '/output/'
    localFileName = localInputFolder + tail

    if (ioOptionsAWS.get_s3_file_dependencies):
        cloud.downloadDirectoryFromS3(ioOptionsAWS.input_s3_bucket_name, ioOptionsAWS.input_s3_region, head, localInputFolder)
    else:
        localFileName = localInputFolder + tail
        print("file downloading: ", ioOptionsAWS.input_s3_key)
        cloud.downloadFileFromS3(ioOptionsAWS.input_s3_bucket_name, ioOptionsAWS.input_s3_region, ioOptionsAWS.input_s3_key, localInputFolder)
    
    return IoLocalOptions(localFileName, localOutputFolder + outputTail)

def uploadFiles(ioOptions, ioOptionsAWS):
    outputHead,outputTail = os.path.split(ioOptionsAWS.output_s3_key)
    local_outputHead,local_outputTail = os.path.split(ioOptions.outputFile)
    cloud.uploadDirectoryToS3(ioOptionsAWS.output_s3_bucket_name, ioOptionsAWS.output_s3_region, local_outputHead+'/', outputHead)


def setGpuAcceleration(useGpuAcceleration):
    if(useGpuAcceleration):
        core.setModuleProperty("Algo", "DisableGPUAlgorithms", "False")
    else:
        core.setModuleProperty("Algo", "DisableGPUAlgorithms", "True")

def convertToMesh_localFile(ioOptions, meshOptions, transformsOptions, useGpuAcceleration=False):
    #ioOptions = IoLocalOptions(ioLocalOptions.inputFile, ioLocalOptions.outputFile)
    setGpuAcceleration(useGpuAcceleration)
    convertToMesh(eval(str(ioOptions)), eval(str(meshOptions)), eval(str(transformsOptions)) )


def convertToMesh_s3File_with_env_var():
    #core.removeConsoleVerbose(2)

     # Env variables override arguments list
    if os.getenv('IO_OPTIONS') is not None:
        ioOptionsAWS = os.getenv('IO_OPTIONS')
        print('Found IO_OPTIONS env var: '+ os.getenv('IO_OPTIONS'))
    else:
        print('IO_OPTIONS env var could not be found. Exiting...')
        return -1
    
    if os.getenv('MESH_OPTIONS') is not None:
        meshOptions = os.getenv('MESH_OPTIONS')
        print('Found MESH_OPTIONS env var: '+ os.getenv('MESH_OPTIONS'))
    else:
        print('MESH_OPTIONS env var could not be found. Exiting...')
        return -1

    if os.getenv('TRANSFORM_OPTIONS') is not None:
        transformsOptions = os.getenv('TRANSFORM_OPTIONS')
        print('Found TRANSFORM_OPTIONS env var: '+ os.getenv('TRANSFORM_OPTIONS'))
    else:
        print('TRANSFORM_OPTIONS env var could not be found. Exiting...')
        return -1
    
    if os.getenv('USE_GPU_ACCELERATION') is not None:
        useGpuAcceleration = str2bool(os.getenv('USE_GPU_ACCELERATION'))
        print('Found USE_GPU_ACCELERATION env var: '+ os.getenv('USE_GPU_ACCELERATION'))
    else:
        useGpuAcceleration = False
        print('USE_GPU_ACCELERATION env var could not be found. Will continue with False value')

  
    ioOptions = downloadFiles(eval(str(ioOptionsAWS)))
    setGpuAcceleration(useGpuAcceleration)
    convertToMesh(ioOptions, eval(str(meshOptions)), eval(str(transformsOptions)) )
    uploadFiles(ioOptions, eval(str(ioOptionsAWS)))



def convertToMesh_s3File_with_arguments(ioOptionsAWS, meshOptions, transformsOptions, useGpuAcceleration=False):
    #core.removeConsoleVerbose(2)

    ioOptions = downloadFiles(eval(str(ioOptionsAWS)))
    setGpuAcceleration(useGpuAcceleration)
    convertToMesh(ioOptions, eval(str(meshOptions)), eval(str(transformsOptions)) )
    uploadFiles(ioOptions, eval(str(ioOptionsAWS)))




def optimizePointCloud_localFile(ioOptions, pointCloudOptions, transformsOptions, useGpuAcceleration=False):
    #ioOptions = IoLocalOptions(ioLocalOptions.inputFile, ioLocalOptions.outputFile)
    setGpuAcceleration(useGpuAcceleration)
    optimizePointCloud(eval(str(ioOptions)), eval(str(pointCloudOptions)), eval(str(transformsOptions)) )


def optimizePointCloud_s3File_with_env_var():
    #core.removeConsoleVerbose(2)

     # Env variables override arguments list
    if os.getenv('IO_OPTIONS') is not None:
        ioOptionsAWS = os.getenv('IO_OPTIONS')
        print('Found IO_OPTIONS env var: '+ os.getenv('IO_OPTIONS'))
    else:
        print('IO_OPTIONS env var could not be found. Exiting...')
        return -1
    
    if os.getenv('POINT_CLOUD_OPTIONS') is not None:
        pointCloudOptions = os.getenv('POINT_CLOUD_OPTIONS')
        print('Found POINT_CLOUD_OPTIONS env var: '+ os.getenv('POINT_CLOUD_OPTIONS'))
    else:
        print('POINT_CLOUD_OPTIONS env var could not be found. Exiting...')
        return -1

    if os.getenv('TRANSFORM_OPTIONS') is not None:
        transformsOptions = os.getenv('TRANSFORM_OPTIONS')
        print('Found TRANSFORM_OPTIONS env var: '+ os.getenv('TRANSFORM_OPTIONS'))
    else:
        print('TRANSFORM_OPTIONS env var could not be found. Exiting...')
        return -1
    
    if os.getenv('USE_GPU_ACCELERATION') is not None:
        useGpuAcceleration = str2bool(os.getenv('USE_GPU_ACCELERATION'))
        print('Found USE_GPU_ACCELERATION env var: '+ os.getenv('USE_GPU_ACCELERATION'))
    else:
        useGpuAcceleration = False
        print('USE_GPU_ACCELERATION env var could not be found. Will continue with False value')

  
    ioOptions = downloadFiles(eval(str(ioOptionsAWS)))
    setGpuAcceleration(useGpuAcceleration)
    optimizePointCloud(ioOptions, eval(str(pointCloudOptions)), eval(str(transformsOptions)) )
    uploadFiles(ioOptions, eval(str(ioOptionsAWS)))



def optimizePointCloud_s3File_with_arguments(ioOptionsAWS, pointCloudOptions, transformsOptions, useGpuAcceleration=False):
    #core.removeConsoleVerbose(2)

    ioOptions = downloadFiles(eval(str(ioOptionsAWS)))
    setGpuAcceleration(useGpuAcceleration)
    optimizePointCloud(ioOptions, eval(str(pointCloudOptions)), eval(str(transformsOptions)) )
    uploadFiles(ioOptions, eval(str(ioOptionsAWS)))
    
