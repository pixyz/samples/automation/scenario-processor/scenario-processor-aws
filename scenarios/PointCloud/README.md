



Pixyz scenarios are pxzext files. Each pxzext file contains a specific 3D data prep recipe that can address from the most generic to the most specific use cases.

This PointScenario.pxzext has to be copied in your Pixyz Scenario Processor container or on your S3 bucket. Please refer to this [documentation](https://gitlab.com/pixyz/samples/automation/scenario-processor-aws#how-to-make-additional-optimization-scenarios-work-on-the-pixyz-scenario-processor-container) for more details on how pxzext scenarios are loaded into the Pixyz Scenario Processor.



# Point Cloud Scenario

This scenario can be used for meshing or decimating point clouds. Per meshing, it is meant for creating an envelop mesh of point clouds. For precise meshing results a high density of points is recommended (the sparser the points are, the less accurate is the mesh)

The point cloud formats supported are **e57, ptx** and **pts** (Pixyz supports Autodesk Recap format but only on Windows and requires users to have a Autodesk Developer Network license)

This scenario has 2 main functions (convert_to_mesh and optimize_point_cloud) with each 3 modes for managing the I/O files strategy.



**CONVERT TO MESH - S3 FILE WITH ENV VAR**

This function will convert a point cloud into a mesh

In that mode, you simply need to set environment variables that can be transmit to the container instead of settings arguments at the end of the container command line. Pixyz scenario will simply ask the OS for environment variables at runtime.

- Image

`709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2021.1.0.79`

- Command

  `--bucket=my-bucket-name,--region=my-region,--scenario=myFolder/PointCloudScenario.pxzext,PointCloudScenario,convertToMesh_s3File_with_env_var`

- Environment variables requested

```bash
IO_OPTIONS
MESH_OPTIONS
TRANSFORM_OPTIONS
```



- IO_OPTIONS

  - Input bucket name (string)
    - Input bucket region (string)
    - Input S3 key (string)
  - Get file dependencies (True or False): if true, all files and subfolders contained at the root level of the input file will be downloaded in the container. This is useful when processing an assembly of files or separated materials/textures. So on S3 bucket, files and dependencies should be stored accordingly.
    - Output bucket name (string)
  - Output bucket region (string)
    - Output S3 key (string): choosing the file extension here will determine the output format from Pixyz. If the resulted file has dependencies (ex: mtl file for obj), they will be uploaded in the same folder

  

  - MESH_OPTIONS

      - Segmentation (int): if > 1, it will split the point cloud into sub parts (can be useful for occlusion culling in a real-time render engine)

    - Grid Resolution ([Int](CoreTypes.html#Int)) :     Resolution of the voxel grid used to generated the proxy (low: 50, medium:100, high:200)

    - generateDiffuseTexture : 

      - bool ('Yes' or 'No')
    - texture baking option:
        - map resolution (low:512, medium:1024, high:2048)
      - padding
      
    

- TRANSFORM_OPTIONS
  
  - Scale (float): scale model
  
    - Orientation ('automaticOrientation' or 'manualOrientation') 
  
      - Automatic orientation:  ["automaticOrientation",0]
  
      - Manual orientation is composed of 2 booleans (leftHanded and zUP) 
  
        - ex: ["manualOrientation",pxz.pointcloudscenario.Orientation(False, True)]
        
        
  
  - Example:

| Env var           | value                                                        |
| ----------------- | ------------------------------------------------------------ |
| IO_OPTIONS        | pxz.pointcloudscenario.IoAwsOptions("my-pixyz-bucket",  "eu-west-3", "inputFolder/inputFile.e57", False,  "my-pixyz-bucket", "eu-west-3",  "outputFolder/outputFile.glb") |
| MESH_OPTIONS      | pxz.pointcloudscenario.MeshOptions(1, 50,  ["Yes",pxz.pointcloudscenario.BakingOptions(1024, 1)]) |
| TRANSFORM_OPTIONS | pxz.pointcloudscenario.TransformsOptions(1.000000,  ["automaticOrientation",0]) |

​	For now the parameterization of this scenario is a little complex. We’ll work on proposing some presets for simpler usage.

​	



**CONVERT TO MESH - S3 JSON MODE**

This function will convert a point cloud into a mesh.

The advantage of this mode is that you can automatically trigger the Pixyz task when a json file comes in the S3 bucket (via Lambda function for instance) without the necessity to communicate scenario settings to the container via a command.



- Image

  `709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2021.1.0.79`

- Container Command

  `--bucket=my-bucket-name, --region=myregion, --json=folder/jsonFile.json`

  The json file you would store on your S3 bucket would look like this:

  ```json
  {
      "scenario": "my-folder/PointCloudScenario.pxzext",  	 
  	"module" : "PointCloudScenario",							
      "function" : "convertToMesh_s3File_with_arguments",						
    	"arguments": {
          "ioOptionsAWS": "pxz.pointcloudscenario.IoAwsOptions('my-pixyz-bucket',  'my-region', 'inputFolder/inputFile.e57', False,  'my-pixyz-bucket', 'my-region',  'outputFolder/outputFile.glb')",
        	"meshOptions": "pxz.pointcloudscenario.MeshOptions(1, 50,  ['Yes',pxz.pointcloudscenario.BakingOptions(1024, 1)])",
          "transformsOptions": "pxz.pointcloudscenario.TransformsOptions(1.000000,  ['automaticOrientation',0])"
      }
  }
  ```

  - scenario: S3 key of the PointCloudScenario.pxzext file

  - module: name of the module (hard coded, do not not change)

  - function: name of the function (leave "convertToMesh_s3File_with_arguments" for this mode)

  - ioOptionsAWS

    - Input bucket name (string)
      - Input bucket region (string)
      - Input S3 key (string)
      - Get file dependencies (True or False): if true, all files and subfolders contained at the root level of the input file will be downloaded in the container. This is useful when processing an assembly of files or separated materials/textures. So on S3 bucket, files and dependencies should be stored accordingly.
      - Output bucket name (string)
      - Output bucket region (string)
      - Output S3 key (string): choosing the file extension here will determine the output format from Pixyz. If the resulted file has dependencies (ex: mtl file for obj), they will be uploaded in the same folder

    

    - meshOptions

      - Segmentation (int): if > 1, it will split the point cloud into sub parts (can be useful for occlusion culling in a real-time render engine)

      - Grid Resolution ([Int](CoreTypes.html#Int)) :     Resolution of the voxel grid used to generated the proxy (low: 50, medium:100, high:200)

      - generateDiffuseTexture : 

        - bool ('Yes' or 'No')
        - texture baking option:
          - map resolution (low:512, medium:1024, high:2048)
          - padding

        

  - transformsOptions

    - Scale (float): scale model

    - Orientation ('automaticOrientation' or 'manualOrientation') 

      - Automatic orientation:  ["automaticOrientation",0]
      - Manual orientation is composed of 2 booleans (leftHanded and zUP) 
        - ex: ["manualOrientation",pxz.pointcloudscenario.Orientation(False, True)]



**CONVERT TO MESH - LOCAL FILE MODE**

This mode lets you convert a point cloud into mesh from local files (files you have mounted  or copied in the Pixyz Scenario Processor container)


  - Image

    `709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2021.1.0.79`

  - Command

    `--bucket=my-bucket-name,--region=my-region,--scenario=myFolder/PointCloudScenario.pxzext,PointCloudScenario,convertToMesh_localFile,"\"pxz.pointcloudscenario.IoLocalOptions('folder/inputFile.e57", 'folder/outputFile.usdz')\"","\"pxz.pointcloudscenario.MeshOptions(1, 50,  ['Yes',pxz.pointcloudscenario.BakingOptions(1024, 1)])\"","\"pxz.pointcloudscenario.TransformsOptions(1.000000,  ['automaticOrientation',0])\"" `

    

    



