



Pixyz scenarios are pxzext files. Each pxzext file contains a specific 3D data prep recipe that can address from the most generic to the most specific use cases.

```
This scenario is already embedded by default in the Pixyz Scenario Processor container
```



# Pxz Converter Scenario



It will simply import a 3D file and save it as a .pxz file, preserving all supported native information contained in the original asset, like B-Rep data contained in CAD assets.

Converting your assets into Pixyz priprietary will allow you to very quickly load your data into other Pixyz products like the Unity plugin for instance, without losing native CAD information.

This scenario has 3 modes for managing the I/O files strategy.



**S3 FILE WITH ARGUMENTS MODE**

- Image

`709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2020.2.3.9`

- Command

`PxzConverter,s3File_with_arguments,"\"inputBucketName\"","\"inputBucketRegion\"","\"input/inputFile.xxx\"",True,`
`"\"outpuBucketName\"","\"outputBucketRegion\"","\"outputFolder/resultFile\""`

- Arguments

  - **PxzConverter**: name of the scenario module

  - **s3File_with_arguments**: Name of the function used inside the scenario module

  - **Input bucket name** (string)

  - **Input bucket region** (string)

  - **Input S3 key** (string)

  - **Get file dependencies** (True or False): if true, all files and
    subfolders contained at the root level of the input file will be
    downloaded in the container. This is useful when processing an
    assembly of files or separated materials/textures. So on S3 bucket, files and dependencies should be stored accordingly.

  - **Output bucket name** (string)

  - **Output bucket region** (string)

  - **Output S3 key** (string): file extension is not needed here as it ill automatically save a .pxz file.





**S3 JSON MODE**

The advantage of this mode is that you can automatically trigger the Pixyz task when a json file comes in the S3 bucket (via Lambda function for instance) without the necessity to communicate scenario settings to the container via a command.



- Image

  `709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2020.2.3.9`

- Container Command

  `--bucket=my-bucket-name, --region=myregion, --json=folder/jsonFile.json`

  The json file you would store on your S3 bucket would look like this:

  ```json
  { 
  	"module" : "PxzConverter",		
      "function" : "s3File_with_arguments",	
    	"arguments": {
          "input_s3_bucket_name": "my-input-bucket-name",
        	"input_s3_region": "eu-west-3",
          "input_s3_key": "input/Meseta_coffee_machine.stp",
          "get_s3_file_dependencies": "True",
          "output_s3_bucket_name": "pixyz-scenario-processor",
          "output_s3_region": "eu-west-3",
          "output_s3_key": "output/result_Meseta_coffee_machine"
      }
  }
  ```





**S3 FILE WITH ENV VAR MODE**

In that mode, you simply need to set environment variables that can be transmit to the container instead of settings arguments at the end of the container command line. Pixyz scenario will simply ask the OS for environment variables at runtime.

- Image

`709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2020.2.3.9`

- Command
  
  `PxzConverter,s3File_with_env_var`
  
- Environment variables requested
```bash
  GET_S3_FILE_DEPENDENCIES
  INPUT_S3_BUCKET_NAME
  INPUT_S3_KEY
  OUTPUT_S3_BUCKET_NAME
  OUTPUT_S3_KEY
```

  



**LOCAL FILE MODE**

This mode uses files you have mounted or copied in the Pixyz Scenario Processor container, and not files stored on S3.

- Image

  `709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2020.2.3.9`
  
- Command

  `PxzConverter,localFile,"\"input/inputFile.xxx\"","\"outputFolder/resultFile\""`

- Arguments
  
  - **PxzConverter**: name of the scenario module
  - **localFile**: Name of the function used inside the scenario module
  - **Input file path** (string)
  - **Output file path** (string): file extension is not needed here as it ill automatically save a .pxz file.
    
    
  
  





