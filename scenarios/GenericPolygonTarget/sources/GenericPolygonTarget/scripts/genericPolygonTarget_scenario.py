import sys
import math
import subprocess
import os
from pxz import core, scene, geom, algo, io, process

PARTS_TARGET        = 256
SIZE_TARGET         = 10000 # in mm
TEXTURE_SIZE_TARGET = [2048, 2048]

PREFER_LOAD_MESH_FORMATS = ['.jt']
POINT_CLOUD_FORMATS = ['.e57', '.pts']
MERGE_FINAL_LEVEL_FORMATS = ['.wire', '.vpb']

class Utils():
    @staticmethod
    def point_sub(pt1, pt2):
        return geom.Point3(pt1.x - pt2.x, pt1.y - pt2.y, pt1.z - pt2.z)

    @staticmethod
    def dot_product(pt1, pt2):
        return pt1.x * pt2.x + pt1.y * pt2.y + pt1.z * pt2.z

    @staticmethod
    def vector_length(v):
        return math.sqrt(Utils.dot_product(v, v))

    @staticmethod
    def get_size(occurrence):
        """
        Returns the bounding box diagonal size in mm
        """
        aabb = scene.getAABB([occurrence])
        return Utils.vector_length(Utils.point_sub(aabb.high, aabb.low))
    
    @staticmethod
    def get_occurrence_center(occurrence):
        """
        Returns the Point3 of the occurrence bounding box center
        """
        aabb = scene.getAABB([occurrence])
        return geom.Point3((aabb.low.x + aabb.high.x)/2, (aabb.low.y + aabb.high.y)/2, (aabb.low.z + aabb.high.z)/2)

def runPolygonTarget(inputFile, output_path, target_polygon, enable_merging, enable_delete):
        root = scene.getRoot()
        stats = list()
    
        splitPath = os.path.splitext(inputFile)
        extension = splitPath[1]

                
        core.setModuleProperty("IO", "LoadPMI", "False")
        core.setModuleProperty('IO', 'LoadHidden', 'False')

        #if (extension in PREFER_LOAD_MESH_FORMATS):
        #    core.setModuleProperty("IO", "PreferLoadMesh", "True")

        importFailed = False
        try:
            # put it in try catch block so we can reset IO module properties
            root = io.importScene(inputFile)
        except Exception as e:
            core.setInteractiveMode(True)
            core.message(e)
            core.setInteractiveMode(False)
            importFailed = True
                
        
        if importFailed: return


        size = Utils.get_size(root)
        
        # point clouds are not prepared the same way as normal files (no tessellation...)
        if extension in POINT_CLOUD_FORMATS: 
            # special preparation for point clouds (proxy mesh generation...)
            root = prepare_point_cloud()
        else: 
            prepare_file(root, size, extension)
        
        print('')
        print('Preparation')
        
        optimize_mesh(root, target_polygon, enable_delete, size)

        if enable_merging:
            root = optimize_product_structure(root)

        # Simple default UV generation        
        algo.mapUvOnAABB([root], False, 300.0, 0, False)

        # Create tangents
        algo.createTangents([root], 0, True)
        
        center_model(root, min(SIZE_TARGET, size)/size)

        print('')
        print('Export')
        if output_path != -1:
            io.exportScene(output_path, root)
            

def is_mesh_optimized(root, target_polygon):
    return scene.getPolygonCount([root], True, False, False) <= target_polygon

def is_product_structure_optimized(root):
    return len(scene.getPartOccurrences(root)) <= PARTS_TARGET

def optimize_mesh(root, target_polygon, enable_delete, size):
    
    if is_mesh_optimized(root, target_polygon): return

    # Decimate quality: medium
    algo.decimate([root], 1.000000, -1, 8.000000, -1, False)
    print('Decimate')

    if is_mesh_optimized(root, target_polygon): return

    if enable_delete:
        # Delete duplicated
        scene.selectDuplicated(0.010000, 0.100000, 0.010000, 0.100000)
        scene.deleteOccurrences(scene.getSelectedOccurrences())
        print('Duplicated')

    if is_mesh_optimized(root, target_polygon): return

    if enable_delete:
        # Delete small parts (less than 2% of global model size)
        scene.selectByMaximumSize([root], size/50, -1.000000)
        scene.deleteSelection()
        print('Small Parts')

    if is_mesh_optimized(root, target_polygon): return

    if enable_delete:
        # Hidden polygons removal: sphere count to 32 to avoid mistakes
        algo.hiddenRemoval([root], 2, 1024, 32, 90.000000, False)
        print('Hidden Parts')

    if is_mesh_optimized(root, target_polygon): return

    # Decimate target to target_polygon
    #algo.decimateTarget([root], ["triangleCount",int(target_polygon)], 1.000000, 1.000000, 1.000000, 1.000000, 10.000000, True, False)
    algo.decimateTarget([root], ["triangleCount",int(target_polygon)], 1, False, 5000000)
    print('Decimate ')

    print('Re-create Normals')
    algo.createNormals([root], -1.000000, True, True)

def optimize_product_structure(root):
    algo.combineMeshesByMaterials([root], True, 0)
    print('Merge')

    # Clean tree
    root = scene.getRoot()
    scene.rake(root, False)
    scene.compress()
    return scene.getRoot()

def center_model(root, scale, convert_to_y_up = False):
    aabb = scene.getAABB([root])
    center = [(x + y)/2 for x, y in zip([aabb.low.x, aabb.low.y, aabb.low.z], [ aabb.high.x, aabb.high.y, aabb.high.z])]
    translationMatrix = [[1, 0, 0, -center[0]],
                            [0, 1, 0, -center[1]],
                            [0, 0, 1, -center[2]],
                            [0, 0, 0, 1]]

    scene.applyTransformation(root, translationMatrix)
    
    if round(scale, 6) != 0.0:
        scaleMatrix = [[scale, 0, 0, 0],
                        [0, scale, 0, 0],
                        [0, 0, scale, 0],
                        [0, 0, 0, 1]]
        scene.applyTransformation(root, scaleMatrix)
        
    if convert_to_y_up:
        scene.applyTransformation(root, [[1, 0, 0, 0], [0, 0, 1, 0], [0, -1, 0, 0], [0, 0, 0, 1]])


def prepare_file(root, size, extension):
    # adaptive size for small objects
    if size < 1000:
        base_size = size/10000
    else: base_size = 0.1

    if extension in MERGE_FINAL_LEVEL_FORMATS:
        scene.mergeFinalLevel([root], 2, True)
    
    algo.repairMesh([root], base_size, True, False)
    algo.createNormals([root], -1.000000, False, False)
    algo.repairCAD([root], base_size, False)
    algo.tessellate([root], base_size*2, -1, -1, True, 0, 1, 0.000000, True, False, True, False)        
    scene.mergeMaterials()
    algo.deletePatches([root], True)
    algo.deleteLines([root])
    algo.deleteFreeVertices([root])

def prepare_point_cloud():
    return process.proxyFromPointCloud(200, ["Yes", process.PointCloudOptions(1024, 1)], False)
