from genericPolygonTarget_scenario import runPolygonTarget
import json
import os

def str2bool(v):
  return v.lower() in ("yes", "true", "t", "1")

def downloadS3Files(input_s3_bucket_name, input_s3_region, input_s3_key, get_s3_file_dependencies, output_s3_bucket_name, output_s3_region, output_s3_key):
    ## S3 files download
    extension = os.path.splitext(input_s3_key)
    head, tail = os.path.split(input_s3_key)
    name = os.path.splitext(tail)

    outputHead, outputTail = os.path.split(output_s3_key)
    outputName = os.path.splitext(outputTail)
    
    localInputFolder = core.getTempDirectory() + '/input/'
    localOutputFolder = core.getTempDirectory() + '/output/'
    localFileName = localInputFolder + tail

    if (get_s3_file_dependencies):
        cloud.downloadDirectoryFromS3(input_s3_bucket_name, input_s3_region, head, localInputFolder)
    else:
        localFileName = localInputFolder + tail
        print("file downloading: ", input_s3_key)
        cloud.downloadFileFromS3(input_s3_bucket_name, input_s3_region, input_s3_key, localInputFolder)
    
    return localFileName, localOutputFolder + outputTail

def uploadS3Files(input_file, outputFile, output_s3_bucket_name, output_s3_region, output_s3_key):
    outputHead,outputTail = os.path.split(output_s3_key)
    local_outputHead,local_outputTail = os.path.split(outputFile)
    cloud.uploadDirectoryToS3(output_s3_bucket_name, output_s3_region, local_outputHead+'/', outputHead)


def setGpuAcceleration(useGpuAcceleration):
    if(useGpuAcceleration):
        core.setModuleProperty("Algo", "DisableGPUAlgorithms", "False")
    else:
        core.setModuleProperty("Algo", "DisableGPUAlgorithms", "True")

def localFile(input_file, output_path, polygon_count_target, do_merge, do_delete_objects, useGpuAcceleration):
    setGpuAcceleration(useGpuAcceleration)
    runPolygonTarget(input_file, output_path, polygon_count_target, do_merge, do_delete_objects)


def s3File_with_arguments(input_s3_bucket_name, input_s3_region, input_s3_key, get_s3_file_dependencies, output_s3_bucket_name, output_s3_region, output_s3_key, polygon_count_target, do_merge, do_delete_objects, useGpuAcceleration):
    #core.removeConsoleVerbose(2)
    input_file, output_path = downloadS3Files(input_s3_bucket_name, input_s3_region, input_s3_key, get_s3_file_dependencies, output_s3_bucket_name, output_s3_region, output_s3_key)
    setGpuAcceleration(useGpuAcceleration)
    runPolygonTarget(input_file, output_path, polygon_count_target, do_merge, do_delete_objects)
    uploadS3Files(input_file, output_path, output_s3_bucket_name, output_s3_region, output_s3_key)
    #core.addConsoleVerbose(2)

def s3File_with_env_var():
    #core.removeConsoleVerbose(2)

    # Env variables override arguments list
    if os.getenv('INPUT_S3_BUCKET_NAME') is not None:
        input_s3_bucket_name = os.getenv('INPUT_S3_BUCKET_NAME')
        print('input_s3_bucket_name argument overriden by INPUT_S3_BUCKET_NAME env var: '+ os.getenv('INPUT_S3_BUCKET_NAME'))
    else:
        print('INPUT_S3_BUCKET_NAME env var could not be found. Exiting...')
        return -1
    
    if os.getenv('INPUT_S3_KEY') is not None:
        input_s3_key = os.getenv('INPUT_S3_KEY')
        print('input_s3_key argument overriden by INPUT_S3_KEY env var: '+ os.getenv('INPUT_S3_KEY'))
    else:
        print('INPUT_S3_KEY env var could not be found. Exiting...')
        return -1
        
    if os.getenv('INPUT_S3_REGION') is not None:
        input_s3_region = os.getenv('INPUT_S3_REGION')
        print('input_s3_region argument overriden by INPUT_S3_REGION env var: '+ os.getenv('INPUT_S3_REGION'))
    else:
        print('INPUT_S3_REGION env var could not be found. Exiting...')
        return -1

    if os.getenv('GET_S3_FILE_DEPENDENCIES') is not None:
        get_s3_file_dependencies = str2bool(os.getenv('GET_S3_FILE_DEPENDENCIES'))
        print('get_s3_file_dependencies argument overriden by GET_S3_FILE_DEPENDENCIES env var: '+ os.getenv('GET_S3_FILE_DEPENDENCIES'))
    else:
        print('GET_S3_FILE_DEPENDENCIES env var could not be found. Exiting...')
        return -1

    if os.getenv('OUTPUT_S3_BUCKET_NAME') is not None:
        output_s3_bucket_name = os.getenv('OUTPUT_S3_BUCKET_NAME')
        print('output_s3_bucket_name argument overriden by OUTPUT_S3_BUCKET_NAME env var: '+ os.getenv('OUTPUT_S3_BUCKET_NAME'))
    else:
        print('OUTPUT_S3_BUCKET_NAME env var could not be found. Exiting...')
        return -1

    if os.getenv('OUTPUT_S3_REGION') is not None:
        output_s3_region = os.getenv('OUTPUT_S3_REGION')
        print('output_s3_region argument overriden by OUTPUT_S3_REGION env var: '+ os.getenv('OUTPUT_S3_REGION'))
    else:
        print('OUTPUT_S3_REGION env var could not be found. Exiting...')
        return -1

    if os.getenv('OUTPUT_S3_KEY') is not None:
        output_s3_key = os.getenv('OUTPUT_S3_KEY')
        print('output_s3_key argument overriden by OUTPUT_S3_KEY env var: '+ os.getenv('OUTPUT_S3_KEY'))
    else:
        print('OUTPUT_S3_KEY env var could not be found. Exiting...')
        return -1

    if os.getenv('POLYGON_COUNT_TARGET') is not None:
        polygon_count_target = int(os.getenv('POLYGON_COUNT_TARGET'))
        print('polygon_count_target argument overriden by POLYGON_COUNT_TARGET env var: '+ os.getenv('POLYGON_COUNT_TARGET'))
    else:
        print('POLYGON_COUNT_TARGET env var could not be found. Exiting...')
        return -1

    if os.getenv('DO_MERGE') is not None:
        do_merge = str2bool(os.getenv('DO_MERGE'))
        print('do_merge argument overriden by DO_MERGE env var: '+ os.getenv('DO_MERGE'))
    else:
        print('DO_MERGE env var could not be found. Exiting...')
        return -1

    if os.getenv('DO_DELETE_OBJECTS') is not None:
        do_delete_objects = str2bool(os.getenv('DO_DELETE_OBJECTS'))
        print('do_delete_objects argument overriden by DO_DELETE_OBJECTS env var: '+ os.getenv('DO_DELETE_OBJECTS'))
    else:
        print('DO_DELETE_OBJECTS env var could not be found. Exiting...')
        return -1

    if os.getenv('USE_GPU_ACCELERATION') is not None:
        useGpuAcceleration = str2bool(os.getenv('USE_GPU_ACCELERATION'))
        print('useGpuAcceleration argument overriden by USE_GPU_ACCELERATION env var: '+ os.getenv('USE_GPU_ACCELERATION'))
    else:
        useGpuAcceleration = False
        print('USE_GPU_ACCELERATION env var could not be found. Will continue with False value')    

    input_file, output_path  = downloadS3Files(input_s3_bucket_name, input_s3_region, input_s3_key, get_s3_file_dependencies, output_s3_bucket_name, output_s3_region, output_s3_key)
    setGpuAcceleration(useGpuAcceleration)
    runPolygonTarget(input_file, output_path, polygon_count_target, do_merge, do_delete_objects)
    uploadS3Files(input_file, output_path, output_s3_bucket_name, output_s3_region, output_s3_key)
    #core.addConsoleVerbose(2)

