



Pixyz scenarios are pxzext files. Each pxzext file contains a specific 3D data prep recipe that can address from the most generic to the most specific use cases.

```
This scenario is already embedded by default in the Pixyz Scenario Processor container
```



# Generic Polygon Target Scenario



This scenario is a kind of "universal" scenario which will optimize your CAD/Point Cloud/Gaming assets and reducing the number of polygons to a target you define in the input parameters. This scenario has 3 modes for managing the I/O files strategy.



**S3 FILE WITH ARGUMENTS MODE**

- Image

709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2021.1.0.79

- Command

`GenericPolygonTarget, s3File_with_arguments,"\"inputBucketName\"","\"inputBucketRegion\"","\"input/inputFile.xxx\"",True,`
`"\"outpuBucketName\"","\"outputBucketRegion\"","\"outputFolder/resultFile.xxx\"",10000,False,False,False`

- Arguments

  - **GenericPolygonTarget**: name of the scenario module
    
  - **s3File_with_arguments**: Name of the function used inside the scenario module
    
  - **Input bucket name** (string)

  - **Input bucket region** (string)

  - **Input S3 key** (string)

  - **Get file dependencies** (True or False): if true, all files and
    subfolders contained at the root level of the input file will be
    downloaded in the container. This is useful when processing an
    assembly of files or separated materials/textures. So on S3 bucket, files and dependencies should be stored accordingly.

  - **Output bucket name** (string)

  - **Output bucket region** (string)

  - **Output S3 key** (string): choosing the file extension here will
    determine the output format from Pixyz. If the resulted file has
    dependencies (ex: mtl file for obj), they will be uploaded in the same
    folder

  - **Polygon count target** (int): the number of final targeted polygons

  - **Do merge** (True or False): Enable product structure optimization
    (reduce draw calls, but combined objects won't move separately)

  - **Do delete objects** (Ture or False): Enable some optimizations that
    will remove objects (delete duplicated, delete small parts, delete
    hidden parts). **Warning: the "delete hidden parts" is GPU
    accelerated**

  - **Use GPU acceleration** (True or False): if true, Pixyz will activate
    GPU acceleration (but if an algorithm requires GPU and no GPU is
    accessible, than the process will fail looking for a missing server X
    connection)

    

**S3 FILE WITH ENV VAR MODE**

In that mode, you simply need to set environment variables that can be transmit to the container instead of settings arguments at the end of the container command line. Pixyz scenario will simply ask the OS for environment variables at runtime.

- Image

709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2021.1.0.79

- Command
  
  `GenericPolygonTarget,s3File_with_env_var`
  
- Environment variables requested
```bash
  DO_DELETE_OBJECTS
  DO_MERGE
  GET_S3_FILE_DEPENDENCIES
  INPUT_S3_BUCKET_NAME
  INPUT_S3_KEY
  OUTPUT_S3_BUCKET_NAME
  OUTPUT_S3_KEY
  POLYGON_COUNT_TARGET
  USE_GPU_ACCELERATION
```

  

**S3 JSON MODE**

The advantage of this mode is that you can automatically trigger the Pixyz task when a json file comes in the S3 bucket (via Lambda function for instance) without the necessity to communicate scenario settings to the container via a command.



- Image

  709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2021.1.0.79

- Container Command

  `--bucket=my-bucket-name, --region=myregion, --json=folder/jsonFile.json`

  The json file you would store on your S3 bucket would look like this:

  ```json
  
  {
      "module" : "GenericPolygonTarget",
      "function" : "s3File_with_arguments",
      "arguments": {
          "input_s3_bucket_name": "'my-input-bucket-name'",
          "input_s3_region": "'eu-west-3'",
          "input_s3_key": "'input/Meseta_coffee_machine.stp'",
          "get_s3_file_dependencies": "False",
          "output_s3_bucket_name": "'my-output-bucket-name'",
          "output_s3_region": "'eu-west-3'",
          "output_s3_key": "'output/result_Meseta_coffee_machine.glb'",
          "polygon_count_target": "10000",
          "do_merge": "True",
          "do_delete_objects": "False",
          "useGpuAcceleration": "False"
      }
  }
  
  ```
  
  Please note that all Python string type variables require single quotes in addition of the double quotes in the JSON "arguments" section description.



**LOCAL FILE MODE**

This mode uses files you have mounted or copied in the Pixyz Scenario Processor container, and not files stored on S3.

- Image

  `709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2020.2.3.9`
  
- Command

  `GenericPolygonTarge,localFile,"\"input/inputFile.xxx\"","\"outputFolder/resultFile.xxx\"",10000,False,False,False`

- Arguments
  
  - **GenericPolygonTarget**: name of the scenario module
  - **localFile**: Name of the function used inside the scenario module
  - **Input file path** (string)
  - **Output file path** (string): choosing the file extension here will
    determine the output format from Pixyz. If the resulted file has
    dependencies (ex: mtl file for obj), they will be uploaded in the same
    folder
  - **Polygon count target** (int): the number of final targeted polygons
  - **Do merge** (True or False): Enable product structure optimization
  (reduce draw calls, but combined objects won't move separately)
  - **Do delete** o**bjects** (Ture or False): Enable some optimizations
    that will remove objects (delete duplicated, delete small parts,
    delete hidden parts). **Warning: the "delete hidden parts" is GPU
    accelerated**
  - **Use GPU acceleration** (True or False): if true, Pixyz will activate
    GPU acceleration (but if an algorithm requires GPU and no GPU is
    accessible, than the process will fail looking for a missing server X
    connection)
  
  

**Schema of the Data Prep Recipe for the Generic Polygon Target Scenario**

!![img/illus_12.png](media/image13.jpeg)



