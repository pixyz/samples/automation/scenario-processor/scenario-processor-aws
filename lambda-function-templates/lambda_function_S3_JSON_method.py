import json
import boto3
import os

# instantiate the AWS SDK
s3_client = boto3.client('s3') 
ecs = boto3.client('ecs')



# this function will specify parameters that are required for the model conversion
# function.  The environment variables are populated by the CloudFormation template
def runEcsTask(bucket, region, key):
    response = ecs.run_task(
        cluster='my-cluster',
        count=1,
        launchType='FARGATE',
        networkConfiguration={
            'awsvpcConfiguration': {
                'subnets': ['subnet-xxxxxxxxxxxx'],
                'securityGroups': ['sg-xxxxxxxxxxxx'],
                'assignPublicIp': 'ENABLED'
            }
        },
        overrides={
            'containerOverrides': [
                {   'name': 'PixyzSP',
                    'command': ["-b"+bucket,"-r"+region,"-j"+key] 
                },
            ],
        },
        taskDefinition='jsonProcessortask:1'
    )
    return response

def lambda_handler(event, context):
    # It is possible for this lambda to be triggered when multiple files are uploaded
    #   so we need to iterate through each file in our triggering event
    for record in event['Records']:
        # extract the S3 bucket name
        # the key is the 'name' of the object uploaded to S3
        bucket = record['s3']['bucket']['name']
        region = os.environ['AWS_REGION']
        key = record['s3']['object']['key']
        print('bucket:' + bucket + ' region:' + region + ' key:' + key)
      
        # run the task.  this occurs asynchronously.
        res = runEcsTask(bucket, region, key)
        print(res)