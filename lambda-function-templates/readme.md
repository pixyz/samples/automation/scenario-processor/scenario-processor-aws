# This folder contains Lambda function sample files



**lambda_function_S3_JSON_method.py**

This lambda function uses the S3 JSON mode of the Scenario Processor container, meaning that the optimization scenario options and input/output files are described in a JSON file and not in a docker command line. This allows you to first upload 3D files and potential dependencies on a S3 bucket and then trigger the Lambda function only when a corresponding JSON arrives in the S3 bucket. One can easily [configure an S3 bucket so it triggers a Lambda function](https://docs.aws.amazon.com/AmazonS3/latest/userguide/enable-event-notifications.html) which will start the ECS task (presuming you have already setup an ESC task and ECS cluster).