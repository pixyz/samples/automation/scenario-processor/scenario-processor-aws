# IMPORTANT UPDATE - MAY 2023
About Pixyz Scenario Processor for Amazon Web Services
Unity has sunset Pixyz Scenario Processor for Amazon Web Services (AWS) and this service is no longer available to new customers. Existing users already having a Pixyz on AWS entitlement can continue using Scenario Processor from their own AWS tenant. Existing scenarios and plugins remain compatible with the existing instances of Scenario Processor for AWS.
If you are interested in a cloud-based transformation solution, a consumption-based solution, or a pay-as-you-go solution, contact our Client Partner team for information about Unity Cloud Platform and its services.

> Pixyz Scenario processor on AWS sunset has no impact in Pixyz Scenario Processor on-prem packages and offer which remain available for purchase from Unity Client Partner teams.

#


![banner](media/banner2.jpg)

[[_TOC_]]

# Description

Pixyz Scenario Processor is a 3D Data Preparation engine capable of automatically importing a wide variety of [3D file formats](https://www.pixyz-software.com/documentations/html/2020.1/studio/SupportedFileFormats.html) (design/engineering and gaming types of asset), optimizing them for real-time or CGI rendering/simulation, and exporting them to any [standard meshed formats](https://www.pixyz-software.com/documentations/html/2020.1/studio/SupportedFileFormats.html).

![img/nutshell.jpg](media/nutshell.jpg)

This container is meant to be used for implementing automated and scalable 3D data preparation workflows on **your** AWS tenant, within AWS ECS and EKS services (It also works on the new [AWS ECS Anywhere](https://aws.amazon.com/ecs/anywhere). Please review this [article](https://aws.amazon.com/fr/blogs/awsmarketplace/deploy-a-containers-based-application-with-aws-marketplace-and-ecs-anywhere/) for more details on how to deploy on ECS Anywhere). 

The container is based on `Ubuntu:focal` Linux distribution and contains the batching tool Pixyz Scenario Processor.

The Pixyz Scenario Processor container can be used in various ways in term of optimization scenarios and I/O data management (further details given later in this documentation, notably serverless architecture with horizontal scalability).



## Run Pixyz Scenario Processor

- from ECS Tasks with Fargate (see [Quick Start](#quick-start))
- from ECS Tasks with EC2 resources (see [Advanced Setup](#advanced-setup))



# Licensing & pricing model

The Pixyz batching tool does not require any additional license server or license file like in other Pixyz products. When Pixyz scenario Processor starts, it will ask AWS Metering service if the user is entitled to use it. Entitlement is procured when the user subscribes in the AWS marketplace to either:

- **Pay-as-you-go**: Just after the Pixyz scenario Processor has launched, the metering clock starts. First minute is indivisible, then pricing is done by the second until the container is stopped. Multiple "Pay-as-you-go" containers will each start their own metering clock.
- **Yearly subscription**:  container can be live 365 days in a row for a fixed price. If using multiple "Yearly" containers at the same time, each will require a distinct Yearly subscription. If you have one Yearly subscription and launch more than one Pixyz Scenario Processor instance at the same time, the first instance will use the yearly subscription and the others will use Pay-as-you-go metering automatically

## About Pixyz Scenario Processor for Amazon Web Services

Unity has sunset Pixyz Scenario Processor for Amazon Web Services (AWS) and is no longer available to new customers. If you are an existing user and already have a Pixyz on AWS entitlement, you can continue to use Scenario Processor from your AWS tenant. Existing scenarios and plugins that you have published from Pixyz Studio 2022.1 remain compatible with the existing instances of Scenario Processor for AWS.

If you are interested in a cloud-based transformation solution, a consumption-based solution, or a pay-as-you-go solution, contact our [Client Partner team](https://create.unity.com/contact-unity-expert) for information about Unity Cloud Platform and its services.

The sunsetting of Pixyz Scenario processor on AWS doesn't affect the on-premises packages and the offering of Pixyz Scenario Processor. To purchase these products, contact the Unity Client Partner team.

# Why should I use Pixyz Scenario Processor? What are the typical use cases?

Pixyz can be used as a backend hub between original 3D data coming from content creation platforms and any 3D experiences. During the last 3 years, it has been extensively used by all size of industrial companies that needed to create automated 3D optimization pipeline.

![img/illus_2.png](media/image4.png)

Pixyz has the capability to enable a wide variety of optimizations scenarios for any kind of end-user devices. You can use the Pixyz Scenario Processor Optimization with all the scenarios plugins provided in this [Gitlab repository](https://gitlab.com/pixyz/samples/automation/scenario-processor-aws/-/tree/master/scenarios) or you can create your own.



Here are a few optimization workflow examples:



## Streamline engineering 3D data to Web/Mobile/HMD  experiences at scale

![img/illus_3.png](media/image5.png)





## Automatically generate marketing 3D data from complex CAD data

![img/illus_4.png](media/image6.png)





## Create optimization pipelines for Gaming/VFX assets

![img/illus_5.png](media/image7.jpg)



# Requirements and limitations



## Prerequisites

- Subscribe to [Pixyz 3D Data Preparation Engine offer](https://aws.amazon.com/marketplace/pp/B08CDYB1ZS) on AWS  Marketplace.
- Setup an S3 bucket and copy your input 3D files (S3 bucket is not mandatory when using the Pixyz Scenario Processor in general, but in this section we will use the `GenricPolygonTarget` scenario in a way that connects to S3 buckets for files I/O management).
- If you want to use GPU resources in order to accelerate Pixyz `Hidden Removal`, `Smart Hidden Removal` or `Smart Orient` algorithms, please refer to the section [About GPU](#about-gpu).



## Limitations

- Paths in container command arguments should not contain spaces

    

- As the container is Linux-based, those input file formats that are only supported on Windows won't be available:
    - Sketchup format
    
    - Autodesk Alias
    
    - Autodesk VRED
    
    - Autodesk Recap
    
      
    
- The (Smart) Hidden Objects Removal functionalities of Pixyz are **GPU-accelerated**.
  A CPU fallback parameter can be activated if no GPU is present on the compute node. CPU fallback processing time can be much longer compared to GPU processing time.

  In case you would build your own scenario in Pixyz Studio, the command to deactivate GPU is `core.setModuleProperty("Algo", "DisableGPUAlgorithms", "True")`

  

- Pixyz Scenario Processor uses a maximum of 32 CPU cores per instance.

- Memory usage depends on the volume of data that the Pixyz Scenario Processor will deal with during the task

    It is not easily predictable but for simplification matter, the number of polygons in your scene will be the major indicator of memory consumption. We recommend sizing a comfortable amount of RAM in order to prevent Pixyz Scenario Processor from exiting during the process due to lack of memory or prevent from performance decreasing.

> ​	As a reference, 1M polygons takes about 0.5Gb of RAM and a 1M points point cloud takes about 50Mb.





# Setup



## Quick Start

### Using Fargate

This quick start assumes you have basic knowledge about how AWS web console and AWS resources work (like IAM roles, EC2 instances, ECS Fargate, EKS clusters...)

1. Subscribe to [Pixyz 3D Data Preparation Engine offer](https://aws.amazon.com/marketplace/pp/B08CDYB1ZS) on AWS Marketplace

    

2. Setup an S3 bucket and copy your input 3D files. As you will reuse later in this quick start your bucket name and input file S3 key, let's say you created a bucket called `my-input-bucket-name` and uploaded a 3D file called `inputFile.step` in a folder `input`

    

3. Create an ECS cluster:
    - From [ECS console](https://us-east-2.console.aws.amazon.com/ecs/home) create a new ECS cluster

    - In Step1, select a **Networking only** cluster

    - In Step2, enable **new VPC**

    - Click `Create`

      

4. From [ECS console](https://us-east-2.console.aws.amazon.com/ecs/home) again, create new `Task Definition`
    - Choose **Fargate** type compatibility

    - In **Task role**, choose an IAM **Elastic Container Service Task** role containing those policies:
      - `AWSMarketplaceMeteringRegisterUsage`
      - `AmazonS3FullAccess`

    - In Task execution Role, choose **create a new role** or select a role containing the `AmazonECSTaskExecutionRolePolicy` policy.

    - Choose **vCPU** unit and **Memory** (see [recommandations](#configuration))

    - Add a container and click on **create**
      - Image is

         709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2021.1.0.79

         **Note**: Replace the version number with the current one.

      - In **Environment**, set the following **command**
        `GenericPolygonTarget,s3File_with_arguments,"my-input-bucket-name", "my-input-bucket-region", "input/inputFile.step",False,"my-output-bucket-name","my-output-bucket-region","outputFolder/resultFile.xxx",10000,False,False,False`
        replace *resultFile.xxx* by the file extension you need, ex: *resultFile.glb* or *resultFile.usdz* 
        (for more details on those arguments, see [Generic Polygon Target Scenario](#generic-polygon-target-scenario))

    - In **Storage an logging**, check Auto-configure CloudWatch logs

      

5. In your **ECS cluster**, click on **Tasks** tab and then click on **Run new tasks**
    - **Launch type**: Fargate

    - Task Definition: your newly created Task

    - Select a subnet

    - Auto-assign public IP: Enabled

    - Click on **Run task**

      

6. Verify the task log from **CloudWatch**
    - Click on the newly created Task

    ![img/illus_6.png](media/image1.png)

    - From the **container** section, click on **View logs in CloudWatch**.

    *Note: In Fargate, the container must be pulled from the container registry each time a Task is triggered. This can take a little bit of time.   During that time you'll see the container status being "Pending".*

    - Cloudwatch logs output should look like this

    ![img/illus_7.png](media/image3.png)

    

7. Once the Task is finished, you will see the result 3D file in your S3 bucket.



## Advanced setup

### With EC2 resources

You can decide to dedicate a specific EC2 instance for your Scenario Processor within your ECS cluster.

EC2 instance with ECS agent will be required in order to be able to communicate with your ECS cluster.

You can either launch an [ECS-optimized EC2 instance](https://aws.amazon.com/fr/premiumsupport/knowledge-center/launch-ecs-optimized-ami/) while you create your ECS cluster, or you can install the ECS agent on your EC2 instance manually ([ECS agent setup tutorial](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-agent-install.html))

1. Create an ECS cluster
    - From [ECS console](https://us-east-2.console.aws.amazon.com/ecs/home), create a new **ECS cluster**

    - In Step1, select a **EC2 Linux + Networking** cluster

    - In Step 2, set mandatory options (instance type, AMI ID...) and then click **Create**

        After a minute or so, your cluster is ready and you can see in the **EC2 instance tab** of your cluster that an EC2 instance is ready

        ![img/illus_8.png](media/image8.png)

    

2. From [ECS console](https://us-east-2.console.aws.amazon.com/ecs/home) again, create new **Task Definition**

    - Choose **EC2 type** compatibility
    - In Task role, choose an IAM **Elastic Container Service Task** role containing following policies
        - `AWSMarketplaceMeteringRegisterUsage`

        - `AmazonS3FullAccess`

    - In Task execution Role, choose **Create new role** or a role containing the policy`AmazonECSTaskExecutionRolePolicy` 
    - Choose **vCPU** unit and **Memory** (see [recommandations](#configuration))
    - Add a container and click on **create**
        - Image is
    
       709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2021.1.0.79

       **Note**: Replace the version number with the current one.
    
    - In **Environment**, set the following **command**
    
    `GenericPolygonTarget,s3File_with_arguments,"my-input-bucket-name", "my-input-bucket-region", "input/inputFile.step",False,"my-output-bucket-name","my-output-bucket-region","outputFolder/resultFile.xxx",10000,False,False,False`
        replace *resultFile.xxx* by the file extension you need, ex: *resultFile.glb* or *resultFile.usdz* 
        (for more details on those arguments, see [Generic Polygon Target Scenario](#generic-polygon-target-scenario))
    
    - In **Storage an logging**, check Auto-configure CloudWatch logs
    
        
    
3. In your **ECS cluster**, click on **Tasks** tab and then click on **Run new tasks**

    - **Launch type**: EC2

    - Task Definition: your newly created Task

    - Click on **Run task**

    

4. Verify the task log from **CloudWatch**

    - Click on the newly created Task

    ![img/illus_6.png](media/image1.png)

    - From the **container** section, click on **View logs in CloudWatch**. 

    *Note: The first time the task is start, it pulls the container from the container registry. This can take a little bit of time. During that time, you'll see the container status being "Pending".*

    - Cloudwatch logs output should look like this

    ![img/illus_7.png](media/image3.png)

    

5. Once the Task is finished, you will see the result 3D file in your S3 bucket.



### With EKS resources

1. Install [AWS CLI](https://docs.aws.amazon.com/eks/latest/userguide/getting-started-eksctl.html), [kubectl](https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/) & [eksctl](https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html)
2. Setup **EKS cluster** and **IAM service account**

    ```bash
    # Create cluster (takes a few minutes)
    eksctl create cluster pixyz-test
    # Associate IAM service account
    eksctl utils associate-iam-oidc-provider --cluster pixyz-test --approve
    eksctl create iamserviceaccount --name pixyz-serviceaccount --namespace default --cluster pixyz-test --attach-policy-arn arn:aws:iam::aws:policy/AWSMarketplaceMeteringRegisterUsage --approve
    ```

    Add additional policies in newly created IAM role if needed:

    - `AmazonEC2ContainerRegistryReadOnly`
    - `AmazonS3FullAccess`
3. Create a local YAML file named `scenario-processor-aws.yml`

    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      name: scenario-processor-aws
    spec:
      ports:
      - port: 3306
      selector:
        app: scenario-processor-aws
      clusterIP: None
     
    ---
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: scenario-processor-aws
    spec:
      selector:
        matchLabels:
          app: scenario-processor-aws
      strategy:
        type: Recreate
      template:
        metadata:
          labels:
            app: scenario-processor-aws
        spec:
          serviceAccountName: pixyz-serviceaccount 
          containers:
          - name: scenario-processor-aws
            image: 709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2021.1.0.79
            args: ["GenericPolygonTarget", "s3File_with_arguments", "\"my-input-bucket-name\"", "\"my-input-bucket-region\"", "\"input/Meseta_coffee_machine.stp\"", "True", "\"my-output-bucket-name\"", "\"my-output-bucket-region\"","\"output/output.glb\"", "10000", "False", "False", "False"]
    ```

4. Deploy and start container on a POD

    ```bash
    aws eks \--region your-region update-kubeconfig \--name pixyz-test
    kubectl apply -f scenario-processor-aws.yml
    ```

5. Log container

    ```bash
    kubectl get pods \--all-namespaces
    kubectl logs scenario-processor-aws-xxxxxxxxx
    ```

6. Delete resources

    ```bash
    kubectl delete -f scenario-processor-aws.yml
    # Detach added policies from the IAM service role
    eksctl delete cluster \--name pixyz-test
    ```



# Configuration



## Sizing vCPU and Memory recommendations

- One instance of Pixyz Scenario Processor is multi-threaded (not multi-processor)
- 1vCPU with 4 to 32 CPU cores is [recommended](https://aws.amazon.com/fr/ec2/physicalcores/)
- Memory usage depends on the volume of data that the Pixyz Scenario Processor will deal with during the task

  It is not easily predictable but for simplification matter, the number of polygons in your scene will be the major indicator of memory consumption. We recommend sizing a comfortable amount of RAM in order to prevent Pixyz Scenario Processor from exiting during the process due to lack of memory.

> ​	As a reference, 1M polygons takes about 0.5Gb of RAM and a point cloud of 1M points takes about 50Mb of RAM





## About GPU

 A few Pixyz algorithms are accelerated on GPU (ray-casting-based algorithms). If there are no GPU, your optimization scenario has to deactivate GPU-accelerated capability in the Pixyz engine. This will activate the CPU fallback for those ray-casting algorithms which can be 100x slower than with a GPU.

 For information, the Pixyz API command to deactivate GPU is:

 `core.setModuleProperty("Algo", "DisableGPUAlgorithms", "True")`

 

 **Activating GPU acceleration**:

 AWS Fargate doesn't support GPU as of today. If you want to use GPU, you may use an ECS ready GPU-type EC2 instances. The AWS community store contains one you can use: 

 - ami-0524951c56448bf78 (amzn2-ami-ecs-gpu-hvm-2.0.20200928-x86_64-ebs) in us-east-1. 

 You can grab the latest AMI for each region using the following AWS CLI command:

 - aws ssm get-parameters --names /aws/service/ecs/optimized-ami/amazon-linux-2/gpu/recommended

 Also your ECS task definition requires that you set a minimum of 1 GPU in the "Environment" section from the Advanced container configuration panel, as well as `DISPLAY=:0` env var.

 

 **List of GPU-accelerated functions in Pixyz Scenario Processor**:

-   Hidden Removal (delete parts, patches or polygons not viewed from a sphere around the scene)
-   Smart Hidden removal (delete parts, patches or polygons not viewed from a set of cameras automatically generated)
-   Smart Orient (reorient mesh faces from camera view points set around the 3D model)



## Scalable Reference architecture

Here is an example of serverless and horizontal scalable architecture. The entire process is automated and can scale to handle multiple files in parallel and runs without the need to provision any servers.

**Cloud formation template and white paper available in this pdf** [here](https://docs.aws.amazon.com/whitepapers/latest/serverless-data-optimization-pipelines-on-aws/welcome.html)





![img/illus_9.png](media/image9.jpg)






# Ready-to-use scenarios

The Pixyz Scenario Processor requires an optimization scenario to work. It is a binary .pxzext file containing a 3D data prep recipe corresponding to specific optimization strategy. 

Multiple optimization strategies can exist because a good optimization strategy will depend on the final purpose of your 3D asset. For instance if you want to do a 360° exterior review experience of a building, you won't need interior parts and for real-time rendering performance reason you'll likely use a scenario capable of automatically removing parts that are not visible from the outside.

**Input 3D files location**

The ready-to-use scenarios provided by Pixyz are able to load 3D files from **volumes mounted** on the container or directly from a **S3 bucket**.

**Input settings configuration**

Globally Pixyz ready-to-use scenarios have 3 ways to declare their user input settings:

1. <u>Using argument list in container command</u>

    - Container command example :

     `--bucket=mybucketname,--region=eu-west-3,--scenario=folder/scenario.pxzext,scenarioModuleName,scenarioFunction,arg1,arg2,...`

    `--bucket`, `-- region` and `--scenario` are optional. They are needed when you want use a scenario file that is stored on a S3 bucket and thus not preinstalled in your container. 

    

2. <u>Using environment variables</u>

    In that case, you simply need to set environment variables that can be transmit to the container instead of settings arguments at the end of the container command line. Pixyz scenario will simply ask the OS for environment variables at runtime.

    - Container command example :

      `--bucket=mybucketname,--region=eu-west-3,--scenario=folder/scenario.pxzext,scenarioModuleName,scenarioFunction`

      

3. <u>Using an external S3 json file containing user input settings</u>

    The advantage of this mode is that you can automatically trigger the Pixyz task when a json file comes in the S3 bucket (via Lambda function for instance) without the necessity to communicate scenario settings to the container via a command.

- Container Command
  
  `--bucket=my-bucket-name, --region=myregion, --json=folder/jsonFile.json`
  
  

​	`--json` is S3 key of json file (string)
​    
​	The json file you would store on your S3 bucket would look like this:
​    
```json
    {
        "scenario": "scenarios/myScenario.pxzext",  		// s3 key of scenario file that will be downloaded 
        "module" : "myScenarioModule",						// name of scenario module
        "function" : "myScenarioFunction",					// name of scenario function
        "arguments": {
            "arg1": "xxx",
            "arg2": "xxx",
            "arg3": "xxx"
        }
    }
```



## Built-in scenarios

The Scenario Processor already embedds a ready-to-use scenario.

### Generic Polygon Target scenario

This is a kind of "universal" scenario which will optimize your CAD/Point Cloud/Gaming assets and reducing the number of polygons to a target you define in the input parameters. This scenario has four modes for managing the I/O files strategy.

- **S3 JSON MODE**

  - Image
  
    709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2021.1.0.79

    **Note**: Replace the version number with the current one.

  - Container Command

    `--bucket=my-bucket-name, --region=myregion, --json=folder/jsonFile.json`

    The json file you would store on your S3 bucket would look like this:

```json
{
    "module" : "GenericPolygonTarget",
    "function" : "s3File_with_arguments",
    "arguments": {
        "input_s3_bucket_name": "'my-input-bucket-name'",
        "input_s3_region": "'eu-west-3'",
        "input_s3_key": "'input/Meseta_coffee_machine.stp'",
        "get_s3_file_dependencies": "False",
        "output_s3_bucket_name": "'my-output-bucket-name'",
        "output_s3_region": "'eu-west-3'",
        "output_s3_key": "'output/result_Meseta_coffee_machine.glb'",
        "polygon_count_target": "10000",
        "do_merge": "True",
        "do_delete_objects": "False",
        "useGpuAcceleration": "False"
    }
}
```

​    Please note that all Python string type variables require single quotes in addition of the double quotes in the JSON  "arguments" section description.

  

- **S3 FILE WITH ARGUMENTS MODE**

  - Image

    709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2021.1.0.79

    **Note**: Replace the version number with the current one.

  - Command

  `GenericPolygonTarget, s3File_with_arguments,"\"inputBucketName\"","\"inputBucketRegion\"","\"input/inputFile.xxx\"",True,`
  `"\"outpuBucketName\"","\"outputBucketRegion\"","\"outputFolder/resultFile.xxx\"",10000,False,False,False`
  
  - Arguments
  
    - **GenericPolygonTarget**: name of the scenario module
      
    - **s3File_with_arguments**: Name of the function used inside the scenario module
      
    - **Input bucket name** (string)
  
    - **Input bucket region** (string)
  
    - **Input S3 key** (string)
  
    - **Get file dependencies** (True or False): if true, all files and
      subfolders contained at the root level of the input file will be
      downloaded in the container. This is useful when processing an
      assembly of files or separated materials/textures. So on S3 bucket, files and dependencies should be stored accordingly.
  
    - **Output bucket name** (string)
  
    - **Output bucket region** (string)
  
    - **Output S3 key** (string): choosing the file extension here will
      determine the output format from Pixyz. If the resulted file has
      dependencies (ex: mtl file for obj), they will be uploaded in the same
      folder
  
    - **Polygon count target** (int): the number of final targeted polygons
  
    - **Do merge** (True or False): Enable product structure optimization
      (reduce draw calls, but combined objects won't move separately)
  
    - **Do delete objects** (Ture or False): Enable some optimizations that
      will remove objects (delete duplicated, delete small parts, delete
      hidden parts). **Warning: the "delete hidden parts" is GPU
      accelerated**
  
    - **Use GPU acceleration** (True or False): if true, Pixyz will activate
      GPU acceleration (but if an algorithm requires GPU and no GPU is
      accessible, than the process will fail looking for a missing server X
      connection)
  
      
  
  **S3 FILE WITH ENV VAR MODE**
  
- Image

  709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2021.1.0.79

  **Note**: Replace the version number with the current one.

- Command
  
  `GenericPolygonTarget,s3File_with_env_var`
  
- Environment variables requested
```bash
  DO_DELETE_OBJECTS
  DO_MERGE
  GET_S3_FILE_DEPENDENCIES
  INPUT_S3_BUCKET_NAME
  INPUT_S3_KEY
  OUTPUT_S3_BUCKET_NAME
  OUTPUT_S3_KEY
  POLYGON_COUNT_TARGET
  USE_GPU_ACCELERATION
```

  

- **LOCAL FILE MODE**

  - Image

    709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2021.1.0.79

    **Note**: Replace the version number with the current one.
    
  - Command
  
    `GenericPolygonTarge,localFile,"\"input/inputFile.xxx\"","\"outputFolder/resultFile.xxx\"",10000,False,False,False`
  
  - Arguments
    
    - **GenericPolygonTarget**: name of the scenario module
    
    - **localFile**: Name of the function used inside the scenario module
    
    - **Input file path** (string)
    
    - **Output file path** (string): choosing the file extension here will
      determine the output format from Pixyz. If the resulted file has
      dependencies (ex: mtl file for obj), they will be uploaded in the same
      folder
    - **Polygon count target** (int): the number of final targeted polygons
    
    - **Do merge** (True or False): Enable product structure optimization
    (reduce draw calls, but combined objects won't move separately)
    
    - **Do delete** o**bjects** (Ture or False): Enable some optimizations
      that will remove objects (delete duplicated, delete small parts,
      delete hidden parts). **Warning: the "delete hidden parts" is GPU
      accelerated**
    - **Use GPU acceleration** (True or False): if true, Pixyz will activate
      GPU acceleration (but if an algorithm requires GPU and no GPU is
      accessible, than the process will fail looking for a missing server X
      connection)





**Schema of the Data Prep Recipe for the Generic Polygon Target Scenario**

![img/illus_12.png](media/image13.jpeg)





## Where can I find additional optimization scenarios?

Pixyz provides additional optimization scenarios on the Pixyz GitLab [here](https://gitlab.com/pixyz/samples/automation/scenario-processor-aws/-/tree/master/scenarios).

Pixyz scenarios are pxzext files. Each pxzext file contains a specific 3D data prep recipe that can address from the most generic to the most specific use cases. We will add new scenarios continuously.

Pixyz can also be contracted for specific scenario development. For more information about Pixyz service offers, please contact [sales](contact@pixyz-software.com) representatives.



## How to make additional optimization scenarios work on the Pixyz Scenario Processor container?

Prior to executing the Pixyz Scenario Processor container, Pixyz scenarios (pxzext files) have to be present in the container folder *`/usr/share/PiXYZScenarioProcessor/plugins`*

You can either:

- Mount a volume on your EC2 instance that contains pxzext files and bind it to the *`/usr/share/PiXYZScenarioProcessor/plugins`* folder. This can be achieved by adding the `-v` argument in the Docker run command. (example: `-v ~/local_scenario_folder:/usr/share/PiXYZScenarioProcessor/plugins`)

- Re-build the Scenario Processor Docker image with a Docker file that will embed your pxzext files:

  `COPY myScenario.pxzext /usr/share/PiXYZScenarioProcessor/plugins/`

- Or the easiest way is to upload your pxzext on an S3 bucket and add an optional arguments in your ECS task command arguments list in order to download and install the remote scenario plugin at launch time.
  -  -b[<arg>] or --bucket[=<arg>]

        -r[<arg>] or --region[=<arg>]

        -s[<arg>] or --scenario [=<arg>]

  ​    

  Example:
  
- `-bmybucketname,-reu-west-3,-sfolder/myscenario.pxzext,myScenarioModuleName,myScenarioFunction,arg1,...`

- `--bucket=mybucketname,--region=eu-west-3,--scenario=folder/myscenario.pxzext,myScenarioModuleName,myScenarioFunction,arg1,...`





## Can I create my own optimization scenarios?

In case you need to create your own 3D data prep recipes, you can create new Pixyz Scenarios (pxzext files) with the tool Pixyz Studio (providing that you have the additional license token "Publish" in your Pixyz Studio license. Please [contact](contact@pixyz-software.com) Pixyz for more information).

- [What is a plugin in Pixyz Studio?](https://pixyz-software.com/documentations/html/2021.1/studio/Plugins.html)
- [How to create a new plugin in Pixyz Studio?](https://pixyz-software.com/documentations/html/2021.1/studio/CreatingaPlugin.html)

Once you have a pxzext file, you will call it and its python function that way:

- `myScenarioModuleName, myScenarioFunction, myScenarioParameter1, myScenarioParameter2...`
- my Scenario and myScenarioFunction names, as well as parameter types are defined in the XML description of your Pixyz Studio plugin, respectively
  `<module>` name, `<function>` name and `<parameter>` types:

  ![img/illus_11.png](media/image12.png)









# Third party licenses contained in Pixyz Scenario Processor

List of 3rd party licenses: [https://www.pixyz-software.com/legal/3rdparty/](https://www.pixyz-software.com/legal/3rdparty/)





# Security Policy of the Pixyz Scenario Processor

Please visit this page [https://www.pixyz-software.com/legal/security-policy/](https://www.pixyz-software.com/legal/security-policy/)





# Lexica

- [AWS Fargate](https://aws.amazon.com/fargate/): serverless compute for containers. Fargate removes the need to provision and manage servers, lets you specify and pay for resources per application, and improves security through application isolation by design
- [AWS EKS](https://aws.amazon.com/eks): fully managed [Kubernetes](https://aws.amazon.com/kubernetes/) service



# Further readings

1. Monitor the Scenario Processor tasks and embed graphs into other pages with Cloud Watch: this [blog article](https://aws.amazon.com/blogs/devops/building-an-amazon-cloudwatch-dashboard-outside-of-the-aws-management-console/) may help illustrate how to do that.
2. EKS
   1. [Getting started with AWS EKS](https://docs.aws.amazon.com/eks/latest/userguide/what-is-eks.html)
   2. [Pixyz Scenario Processor requires EKS service account IAM role setup](https://aws.amazon.com/blogs/opensource/introducing-fine-grained-iam-roles-service-accounts/)
3. [AWS Batch](https://docs.aws.amazon.com/batch/latest/userguide/get-set-up-for-aws-batch.html)





# Troubleshooting

- "I have a `Register error: curlCode: 28, Timeout was reached`"
  This likely means that your VM cannot reach the AWS Metering service. If you're using an ECS Task, ensure that the Auto-assign public IP is ENABLED, or enable Network mode "Bridge" from the ECS task definition

-   "`mutex lock error` happens at the end of the process" 

  This is a known issue. The process was successful but the container is not exiting correctly and throw this kind of errors. This issue happens at the end of your process, it has no impact on your processed files.
  
- "I'm seeing some delays, which we think are blocked messages caused by my firewall."

  This may be due to analytics messages that you can disable by using this command in a custom scenario plugin `core.setModuleProperty("Core", "EnableAnalytics", "False")`





# More information about Pixyz

- [Website](https://www.pixyz-software.com/)
- [Support](https://www.pixyz-software.com/support/)
