#################################################
## Create An ECS Task Definition Using AWS CLI ##
#################################################
## In case you prefer using the AWS web console or for more information, please refer to this documentation: https://gitlab.com/pixyz/samples/batch/scenario-processor-aws

## Prerequisite: AWS CLI installed and configured with proper access


## Get task definition file template for reference
## Will list all available parameters in task definition
aws ecs register-task-definition \
--generate-cli-skeleton


## Create ECS task execution role
vi my-task-execution-role.json
-----------------------
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
-----------------------
:wq

## Create the task execution role
aws iam --region us-west-2 create-role --role-name ecsTaskExecutionRole --assume-role-policy-document file://my-task-execution-role.json

## Attach the needed task execution role policies
aws iam --region us-west-2 attach-role-policy --role-name ecsTaskExecutionRole --policy-arn arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy



## Create ECS Pixyz container task role
vi my-pixyz-task-role.json
-----------------------
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
-----------------------
:wq

## Create the Pixyz task role
aws iam --region us-west-2 create-role --role-name ecsPixyzTaskRole --assume-role-policy-document file://my-pixyz-task-role.json

## Attach the needed task role policies
aws iam --region us-west-2 attach-role-policy --role-name ecsPixyzTaskRole --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess
aws iam --region us-west-2 attach-role-policy --role-name ecsPixyzTaskRole --policy-arn arn:aws:iam::aws:policy/AWSMarketplaceMeteringRegisterUsage



## Create a task definition file
vi my-task-definition.json
-----------------------
{
  "family": "mytaskdefinition",
  "networkMode": "awsvpc",
  "executionRoleArn": "arn:aws:iam::xxxxxxxxxxx:role/ecsTaskExecutionRole",
  "taskRoleArn": "arn:aws:iam::xxxxxxxxxxx:role/ecsPixyzTaskRole",
  "containerDefinitions": [{
    "name": "myappv1",
    "image": "709825985650.dkr.ecr.us-east-1.amazonaws.com/pixyz-software/scenario-processor:2021.1.0.79",
    "essential": true,
    "command": [
      "GenericPolygonTarget",
      "s3File_with_arguments",
      "\"my-input-bucket-name\"",
      "\"input/inputFile.xxx\"",
      "False",
      "\"my-output-bucket-name\"",
      "\"output/outputFile.xxx\"",
      "10000",
      "False",
      "False",
      "False"
    ],
   "resourceRequirements": [
      {
        "type": "GPU",
        "value": "1"
      }
    ]
  }],
  "requiresCompatibilities": [
    "EC2"
  ],
  "cpu": "1024",
  "memory": "5120",
  "logConfiguration": {
    "secretOptions": [], 
    "logDriver": "awslogs", 
    "options": {
        "awslogs-region": "us-east-1", 
        "awslogs-stream-prefix": "ecs", 
        "awslogs-group": "/ecs/pixyz"
    }

}
-----------------------
:wq

## Register the task definition
aws ecs register-task-definition \
--cli-input-json file://my-task-definition.json






############################
## Other useful commands ## 
############################


## Get current list of task definitions
aws ecs list-task-definitions 

## Get the list of active task definition families
aws ecs list-task-definition-families \
--status ACTIVE

## Get details of the task definition
aws ecs describe-task-definition \
--task-definition mytaskdefinition

## Observe "revision": 1, when you create the task definition for the 1st time, aws assigns 
## revision 1 to your task definition. This revision will be incremented by one every time you 
## update the same task definition

## Update the my-task-definition.json file and change name to myappv2 from myappv1

## Register the task definition with update
aws ecs register-task-definition \
--cli-input-json file://my-task-definition.json

## Get details of the task definition
aws ecs describe-task-definition \
--task-definition mytaskdefinition:1 #for 1st revision (name: myappv1)

aws ecs describe-task-definition \
--task-definition mytaskdefinition:2 #for 2nd revision (name: myappv2)

## Deregister the task definitions
aws ecs deregister-task-definition \
--task-definition mytaskdefinition:1 &&
aws ecs deregister-task-definition \